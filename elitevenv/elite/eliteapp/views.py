from itertools import product
from django.shortcuts import Http404, render, redirect
from stripe.api_resources import payment_method
from .models import ProductCategery, AddProduct, Cart, Order, WhistList, WhistList
from django.shortcuts import get_object_or_404
from django.http import Http404, HttpResponse, JsonResponse
from django.db.models import F
from .forms import NewUserForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
import stripe
import json
from django.http import JsonResponse
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash

# from django.db.models import Q

# Create your views here.


# home view
def home(request):
    pro = AddProduct.objects.all()
    cat = ProductCategery.objects.all()
    context = {
        "pro": pro,
        "cat": cat,
    }

    return render(request, "home.html", context)


# category
def category_data(request, pk):
    data = get_object_or_404(ProductCategery, pk=pk)
    get_product = AddProduct.objects.filter(category=data)
    context = {
        "data": data,
        "get_product": get_product,
    }
    return render(request, "category.html", context)


# search bar for product searching
def searchbar(request):
    if request.method == "POST":
        search = request.POST.get("searchbar")
        search_results = AddProduct.objects.filter(name__icontains=search)
        context = {
            "search": search_results,
        }
        return render(request, "search_result.html", context)

    return render(request, "search_result.html")


# Static data
def static_data(request, pk):
    data = AddProduct.objects.filter(pk=pk)
    context = {"data": data}

    return render(request, "static_data.html", context)


# cart sectio
@login_required(login_url="signin")
# geting cart data
def cart(request):
    cart_items = Cart.objects.filter(custamer=request.user)
    context = {
        "cart_items": cart_items,
    }
    return render(request, "cart.html", context)


# add product by user in cart
# @login_required(login_url="signin")
def add_to_cart(request, pk):
    try:
        if not request.user.is_authenticated:
            if add_to_cart:
                messages.info(
                    request, "You need to signin then you can use cart ")
                return redirect("signin")
        # if user is authenticated then save order
        # adding item in cart and also checking if item is in then  do not again add
        else:
            get_item = get_object_or_404(AddProduct, pk=pk)

            if Cart.objects.filter(custamer=request.user, product=get_item).exists():
                messages.warning(request, "Product already in cart")

            else:
                save_cart = Cart(product=get_item,
                                 custamer=request.user).save()
                messages.success(request, "Product has been added")

    except:
        messages.error(request, "some thing went wrong Pleases try again")
    return render(request, "partial/messages.html")


# cart quantity increases and decreases


# decreases quantity
def product_quantity_decreases(request, pk):
    Cart.objects.filter(pk=pk).update(quantity=F("quantity") - 1)

    quantity = Cart.objects.filter(pk=pk)

    context = {
        "quantity": quantity,
    }
    return render(request, "partial/quantity.html", context)


# increases quantity
def product_quantity_increases(request, pk):
    quan = Cart.objects.filter(pk=pk).update(quantity=F("quantity") + 1)
    quantity = Cart.objects.filter(pk=pk)

    context = {
        "quantity": quantity,
    }

    return render(request, "partial/quantity.html", context)


# delete cart items
def delete_cart_item(request, pk):
    item_delete = Cart.objects.get(pk=pk).delete()
    # item = Cart.objects.filter(id=id)

    return render(request, "cart.html")


# new user creation
# user form


def signup(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()  # new user has been saved and messages has been send
            messages.success(
                request, "Your account has been created successfully")
            return redirect("signin")
        else:
            messages.error(
                request, "Unsucccessfull registration. Invalid information")
    else:
        form = NewUserForm()

    return render(request, "signup.html", {"form": form})


# signin func
def signin(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                messages.error(request, "Invalid user")
        else:
            messages.error(request, "Invalid username or password")
    form = AuthenticationForm()
    context = {
        "signin_form": form,
    }
    return render(request, "signin.html", context)


# signout
def signout(request):
    logout(request)
    if logout:
        messages.success(request, "You are successfully logout")
    return redirect("signin")


# checkout session
def checkout(request):
    # get cart items using user
    get_cart = Cart.objects.filter(custamer=request.user)
    # get country list
    country = Order.objects.all()

    return render(
        request, "checkout.html", {
            "get_cart": get_cart, "country_list": country}
    )


# save oreder and payment
stripe.api_key = "sk_test_51LlQlHC3Fkc7HuSz8duRwVmTXxUukKqzWHgBxwdwOWJzNWOFzYrGJjZbp8zgqaDLcRQg8200nNsR4vYIu9N7EkPW00LibCeRWe"

def create_checkout_session(request, pk):
    # product_id = kwargs[pk]
    item = get_object_or_404(Cart, pk=pk)
    # get order information forms
    if request.method == "POST":
        name = request.POST.get("name")
        contact = request.POST.get("contact")
        zip = request.POST.get("zip")
        email = request.POST.get("email")
        address = request.POST.get("address")
        form_save = Order(
            name=name,
            contact=contact,
            zip=zip,
            email=email,
            address=address,
            payment_complete=False,
            order_complete=False,
            user=request.user,
        ).save()

    YOUR_DOMAIN = "http://127.0.0.1:8000/"
    # create stripe checkout session

    checkout_session = stripe.checkout.Session.create(
        payment_method_types=["card"],
        line_items=[
            {
                "price_data": {
                    "currency": "usd",
                    "unit_amount": item.product.product_price,
                    "product_data": {
                        "name": item.product.name,
                    },
                },
                "quantity": item.quantity,
            },
        ],
        # payment_method_types: ["card"],
        mode="payment",
        success_url=YOUR_DOMAIN + "success_url",
        cancel_url=YOUR_DOMAIN + "stripe/cancel.html",
    )

    return redirect(checkout_session.url, "stripe/checkout.html")


# return JsonResponse({'checkout_session':checkout_session},)

#
#
#    print(item)
#    save_product = Order(product=item.product, user=request.user ,name=name, contact=contact, email=email, address=address, zip=zip)
#    save_product.save() # order has been saved
#
#


# Payment successfull url
def success_url(request):
    # order information form data save
    order = Order.objects.filter(
        user=request.user).update(payment_complete=True)
    # delete cart after successfully paymeent done
    cart_items = Cart.objects.filter(custamer=request.user).delete()
    print("Payment success full")

    return render(request, "stripe/success.html")


# cancModelsel stripe payment url
def cancel_url(request):
    return render(request, "stripe/cancel.html")


def user_page(request, id):
    user_and_order = Order.objects.filter(user=request.user)
    context = {
        "current_user": current_user,
        "user_and_order": user_and_order,
    }

    return render(request, "user/user.html", context)

#change forgot password
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success("Password has change successfull")
    else:
        form = PasswordChangeForm(user=request.user)
    return render(request,"change_password.html", {"form":form})

# whist list func
def wishlist(request):
    """
    get the all using wish list data
    """

    wish_items = WhistList.objects.filter(user=request.user)
    return render(request, "wishlist.html", {"wish_items":wish_items})

# add product to wishlist
def add_to_wishlist(request, pk):
    """
    this func add product in whishlist
    """
    try:
        if request.method == "POST":
            product = AddProduct.objects.get(pk=pk) 
            create_whistlist = WhistList.objects.create(product=product, user=request.user) 
            messages.success(request, "Item has beem add to whist list")
    except:
        messages.error(request, "Some thing went wrong")
    return render(request, "partial/messages.html")
