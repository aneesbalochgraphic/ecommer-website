# Generated by Django 4.1.3 on 2022-11-16 08:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eliteapp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addproduct',
            name='product_price',
            field=models.IntegerField(),
        ),
    ]
