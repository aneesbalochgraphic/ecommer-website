from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("", views.home, name="home"),
    path("category_data/<int:pk>/", views.category_data, name="category_data"),
    path("searchbar", views.searchbar, name="searchbar"),
    path("static_data/<int:pk>/", views.static_data, name="static_data"),
    path("cart", views.cart, name="cart"),
    path("add_to_cart/<int:pk>/", views.add_to_cart, name="add_to_cart"),
    path("product_quantity_decreases/<int:pk>/", views.product_quantity_decreases, name="product_quantity_decreases"),
    path("product_quantity_increases/<int:pk>/", views.product_quantity_increases, name="product_quantity_increases"),
    path("delete/<int:pk>/", views.delete_cart_item, name="delete_cart_item"), #cart item delete
    path("signup", views.signup, name="signup"), #authencate system
    path("signin", views.signin, name="signin"),
    path("signout", views.signout, name="signout"),
    # send user on in checkout session
    path("checkout", views.checkout, name="checkout"),
    path("create-checkout-session/<str:pk>/", views.create_checkout_session, name="create_checkout_session"),
    path("success_url", views.success_url, name="success_url"),
    path("cancel_url", views.cancel_url, name="cancel_url"),
	path("user_page/<str:id>/", views.user_page, name="user_page"),
    path("change_password/", views.change_password, name="change_password"),
    path("add_to_wishlist/<int:pk>/", views.add_to_wishlist, name="add_to_wishlist"),
    path("wishlist", views.wishlist, name="wishlist"),


] + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT) #static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) # images and static files settings
