from django.db import models
from django.contrib.auth.models import User
from django.db.models.fields import related
from django.db.models.sql.query import product

# Create your models here.


# catogery models
class ProductCategery(models.Model):
    create_category = models.CharField(max_length=500)

    def __str__(self):
        return self.create_category


# product model
class AddProduct(models.Model):
    category = models.ForeignKey(
        ProductCategery,
        on_delete=models.CASCADE,
        related_name="addproducts",
        null=True,
        blank=True,
    )
    name = models.CharField(max_length=150)
    product_price = models.IntegerField(blank=False)
    product_image = models.ImageField(upload_to="static/image", blank=True)
    product_detail = models.TextField(blank=True)
    product_detatil_images = models.ImageField(upload_to="detailimages", blank=True)

    def __str__(self):
        return self.name


# wishlist model
class WhistList(models.Model):
    product = models.ForeignKey(AddProduct, on_delete=models.CASCADE, related_name="whist_list")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.product.name

# cart
class Cart(models.Model):
    product = models.ForeignKey(
        AddProduct, on_delete=models.CASCADE, related_name="custamer_products"
    )
    custamer = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="custamers"
    )
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return self.product.name

    def get_totel_price(self):
        totel_price = self.quantity * self.product.product_price
        return totel_price


#    def get_totel_product_price(self):
#        return self.quantity * self.product.product_price

# order model
# creating choice data
country = (
    ("PAK", "Pakistan"),
    ("BAN", "Bangledesh"),
    ("CHI", "China"),
)


class Order(models.Model):
    product = models.ForeignKey(
        AddProduct,
        on_delete=models.CASCADE,
        related_name="Order",
        null=True,
        blank=True,
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="user", null=True, blank=True
    )
    name = models.CharField(max_length=300, null=False)
    contact = models.IntegerField(blank=True)
    email = models.EmailField(null=False)
    address = models.TextField(blank=True)
    zip = models.IntegerField(blank=True)
#    country = models.CharField(max_length=50, choices=country)
    order_complete = models.BooleanField(default=False)
    payment_complete = models.BooleanField(default=False)
