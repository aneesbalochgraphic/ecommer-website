from django.contrib import admin
from . models import AddProduct, ProductCategery,Cart, Order, WhistList 
# Register your models here.
admin.site.register(AddProduct)
admin.site.register(ProductCategery)
admin.site.register(Cart)
admin.site.register(Order)
admin.site.register(WhistList)

